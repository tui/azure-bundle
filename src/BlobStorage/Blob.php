<?php

namespace Tui\AzureBundle\BlobStorage;

class Blob
{
    /** @var string */
    public $name;

    /** @var array */
    public $metadata;

    /** @var \MicrosoftAzure\Storage\Blob\Models\BlobProperties */
    public $properties;

    /**
     * Gets blob name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Sets blob name
     *
     * @return Blob
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets blob metadata
     *
     * @return array
     */
    public function getMetadata(): array
    {
        return $this->metadata;
    }

    /**
     * Sets blob metadata
     *
     * @return Blob
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Gets blob properties
     *
     * @return BlobProperties
     */
    public function getProperties(): \MicrosoftAzure\Storage\Blob\Models\BlobProperties
    {
        return $this->properties;
    }

    /**
     * Sets blob properties
     *
     * @return Blob
     */
    public function setProperties($properties)
    {
        $this->properties = $properties;

        return $this;
    }
}
