<?php

namespace Tui\AzureBundle\BlobStorage;

class Parameters
{
    /** @var string */
    protected $container;

    /** @var string */
    protected $protocol;

    /** @var string */
    protected $accountName;

    /** @var string */
    protected $accountKey;

    /**
     * Parameters constructor
     * 
     * @param string $container
     * @param string $protocol
     * @param string $accountName
     * @param string $accountKey
     */
    public function __construct($container, $protocol, $accountName, $accountKey)
    {
        $this->container = $container;
        $this->protocol = $protocol;
        $this->accountName = $accountName;
        $this->accountKey = $accountKey;
    }

    /**
     * Gets container name
     *
     * @return string
     */
    public function getContainer(): string
    {
        return $this->container;
    }

    /**
     * Gets connection protocol
     *
     * @return string
     */
    public function getProtocol(): string
    {
        return $this->protocol;
    }

    /**
     * Gets account name
     *
     * @return string
     */
    public function getAccountName(): string
    {
        return $this->accountName;
    }

    /**
     * Gets account key
     *
     * @return string
     */
    public function getAccountKey(): string
    {
        return $this->accountKey;
    }

    /**
     * Gets Azure connection string
     *
     * @return string
     */
    public function getConnectionString(): string
    {
        return sprintf(
            'DefaultEndpointsProtocol=%s;AccountName=%s;AccountKey=%s;',
            $this->protocol, $this->accountName, $this->accountKey
        );
    }

    /**
     * Gets array of connection parameters
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'container' => $this->container,
            'protocol' => $this->protocol,
            'accountName' => $this->accountName,
            'accountKey' => $this->accountKey,
            'connectionString' => $this->getConnectionString(),
        ];
    }
}
