<?php

namespace Tui\AzureBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('tui_azure');

        $rootNode
            ->children()
                ->scalarNode('blob_container')->isRequired()->end()
                ->scalarNode('blob_protocol')->end()
                ->scalarNode('blob_account_name')->isRequired()->end()
                ->scalarNode('blob_account_key')->isRequired()->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
