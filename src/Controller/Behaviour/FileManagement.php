<?php

namespace Tui\AzureBundle\Controller\Behaviour;

use Ramsey\Uuid\Uuid;

trait FileManagement
{
    /**
     * Generate a unique filename based on $file extension
     *
     * @param string $file
     *
     * @return string
     */
    public function generateFilename($file): string
    {
        $filepath = pathinfo($file);
        $ext = $filepath['extension'];

        $filename = sprintf('%s.%s', Uuid::uuid4()->toString(), $ext);

        return $filename;
    }
}
