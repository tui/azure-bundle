<?php

namespace Tui\AzureBundle\Controller\Behaviour;

use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;
use Tui\AzureBundle\BlobStorage\Blob;

trait BlobStorageResponse
{
    /**
     * Process ListBlobsResult object and return array of Blob objects
     * suitable for json output
     *
     * @param ListBlobsResult $results
     *
     * @return array
     */
    public function processBlobsResult(\MicrosoftAzure\Storage\Blob\Models\ListBlobsResult $results): array
    {
        $blobs = [];

        foreach ($results->getBlobs() as $result) {
            $blob = new Blob();

            $blob
                ->setName($result->getName())
                ->setMetadata($result->getMetadata())
                ->setProperties($result->getProperties());

            $blobs[] = $blob;
        }

        return $blobs;
    }

    /**
     * Process GetBlobResult and return Blob object suitable for json output
     *
     * @param  \MicrosoftAzure\Storage\Blob\Models\GetBlobResult $result
     * @param  string $file
     *
     * @return Blob
     */
    public function processBlobResult(\MicrosoftAzure\Storage\Blob\Models\GetBlobResult $result, $file): Blob
    {
        $blob = new Blob();

        $blob
            ->setName($file)
            ->setMetadata($result->getMetadata())
            ->setProperties($result->getProperties());

        return $blob;
    }

    /**
     * Return json encoded ServiceException error
     *
     * @param  ServiceException $e
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function throwError(ServiceException $e): \Symfony\Component\HttpFoundation\JsonResponse
    {
        return $this->json(['error' => $e->getResponse()->getReasonPhrase()], $e->getResponse()->getStatusCode());
    }
}
