<?php

namespace Tui\AzureBundle\Controller;

use MicrosoftAzure\Storage\Blob\BlobRestProxy;
use MicrosoftAzure\Storage\Blob\BlobSharedAccessSignatureHelper;
use MicrosoftAzure\Storage\Blob\Models\ListBlobsOptions;
use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;
use MicrosoftAzure\Storage\Common\Internal\Resources;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Tui\AzureBundle\BlobStorage\Parameters;

/**
 * Controller for Blob Storage API
 *
 * @Route("/blob")
 */
class BlobStorageController extends AbstractController
{
    use Behaviour\BlobStorageResponse;
    use Behaviour\FileManagement;

    /** @var Parameters $parameters */
    protected $parameters;

    /** @var BlobRestProxy */
    protected $blobClient;

    /**
     * Controller constructor
     *
     * @param Parameters $parameters
     */
    public function __construct(
        Parameters $parameters
    ) {
        $this->parameters = $parameters;

        $this->blobClient = BlobRestProxy::createBlobService($parameters->getConnectionString());
    }

    /**
     * List contents of Blob container
     *
     * @param  Request $request
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("", name="blob_storage_list", methods={"GET"})
     */
    public function list(Request $request)
    {
        $prefix = $request->query->get('prefix', null);
        $maxResults = $request->query->get('maxResults', null);

        $listBlobsOptions = new ListBlobsOptions();

        // Add prefix to request if specified in parameters
        if (!is_null($prefix)) {
            $listBlobsOptions->setPrefix($prefix);
        }

        // Add max results to request if specified in parameters
        if (!is_null($maxResults)) {
            $listBlobsOptions->setMaxResults($maxResults);
        }

        try {
            $result = $this->blobClient->listBlobs($this->parameters->getContainer(), $listBlobsOptions);
        } catch (ServiceException $e) {
            return $this->throwError($e);
        }

        return $this->json($this->processBlobsResult($result));
    }

    /**
     * Return object data for Blob
     *
     * @param  Request $request
     * @param  string  $file
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/{file}", name="blob_storage_retrieve", methods={"GET"})
     */
    public function retrieve(Request $request, $file)
    {
        try {
            $blob = $this->blobClient->getBlob($this->parameters->getContainer(), $file);
        } catch (ServiceException $e) {
            return $this->throwError($e);
        }

        return $this->json($this->processBlobResult($blob, $file));
    }

    /**
     * Upload file to Blob storage
     *
     * @param  Request $request
     * @param  string  $file
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/{file}", name="blob_storage_upload", methods={"POST"})
     */
    public function upload(Request $request, $file)
    {
        $content = $request->getContent();

        $filename = $this->generateFilename($file);

        try {
            $result = $this->blobClient->createBlockBlob($this->parameters->getContainer(), $filename, $content);
        } catch (ServiceException $e) {
            return $this->throwError($e);
        }

        return $this->retrieve($request, $filename);
    }

    /**
     * Download file from Blob storage
     *
     * @param  Request $request
     * @param  string  $file
     *
     * @return application/octet-stream
     *
     * @Route("/{file}/download", name="blob_storage_download", methods={"GET"})
     */
    public function download(Request $request, $file)
    {
        try {
            $blob = $this->blobClient->getBlob($this->parameters->getContainer(), $file);
        } catch (ServiceException $e) {
            return $this->throwError($e);
        }

        $contentType = $blob->getProperties()->getContentType();
        $contentLength = $blob->getProperties()->getContentLength();

        header("Content-Disposition: attachment; filename=$file;");
        header("Content-Type: $contentType");
        header("Content-Length: $contentLength");

        return fpassthru($blob->getContentStream());
    }

    /**
     * Get signed GET URL for Blob
     *
     * @param  Request $request
     * @param  string  $file
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/{file}/signget", name="blob_storage_signed_get", methods={"GET"})
     */
    public function signedGet(Request $request, $file)
    {
        return $this->generateSignedUrl($file, 'read');
    }

    /**
     * Get signed PUT URL for Blob
     *
     * @param  Request $request
     * @param  string  $file
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/{file}/signput", name="blob_storage_signed_put", methods={"GET"})
     */
    public function signedPut(Request $request, $file)
    {
        return $this->generateSignedUrl($file, 'write');
    }

    /**
     * Generate read/write signed URL
     *
     * @param string $file
     * @param string $mode
     *
     * @return json
     */
    protected function generateSignedUrl($file, $mode)
    {
        $tokenExpiry = new \DateTime();
        $tokenExpiry->modify("+1 hour");

        if ($mode == 'write') {
            $file = $this->generateFilename($file);
            $permissions = 'w';
        } else {
            $permissions = 'r';
        }

        $resourceName = $this->parameters->getContainer() . '/' . $file;

        $sas = new BlobSharedAccessSignatureHelper($this->parameters->getAccountName(), $this->parameters->getAccountKey());

        // Generate SAS token for read/write access to Blob, expiring in one hour
        $token = $sas->generateBlobServiceSharedAccessSignatureToken(
            Resources::RESOURCE_TYPE_BLOB,
            $resourceName,
            $permissions,
            $tokenExpiry,
            '',
            '',
            'https'
        );

        // Generate full GET/PUT URL for container using token
        $url = sprintf('https://%s.blob.core.windows.net/%s/%s?%s',
            $this->parameters->getAccountName(),
            $this->parameters->getContainer(),
            $file,
            $token
        );

        return $this->json(['url' => $url, 'filename' => $file]);
    }
}
