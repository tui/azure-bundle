# Tui Azure Bundle

This is a bundle for Symfony 4 that provides an API for Microsoft Azure services. Currently this is limited to Blob Storage functions, but may be expanded in the future.

## Installation

First, create the file `config/packages/tui_azure.yaml` with the following content (*TO BE AUTOMATED AT A LATER DATE*):

```
tui_azure:
    blob_container: '%env(TUI_AZURE_BLOB_CONTAINER)%'
    blob_protocol: https
    blob_account_name: '%env(TUI_AZURE_BLOB_ACCOUNT_NAME)%'
    blob_account_key: '%env(TUI_AZURE_BLOB_ACCOUNT_KEY)%'
```

Add the private repository to your `composer.json` file:

```
"repositories": [
    {
        "type": "vcs",
        "url": "git@bitbucket.org:tui/azure-bundle"
    }
]
```

While this bundle is still in development you will also need to specify your minimum stability:

```
"minimum-stability": "dev"
```

And then `composer require tui/azure-bundle`. As it's a private repository you should have an SSH key for your Bitbucket account.

Finally, add a route for the Azure API in your `config/routes.yaml` file:

```
tui_upload:
    resource: "@TuiAzureBundle/Controller/"
    type:     annotation
    prefix:   /api/azure
```

Get the values for `TUI_AZURE_BLOB_CONTAINER`, `TUI_AZURE_BLOB_ACCOUNT_NAME` and `TUI_AZURE_BLOB_ACCOUNT_KEY` from the Azure management console and make them available as environment variables (set in docker-compose, etc).

## Blob Storage API

The bundle provides API endpoints for listing the contents of a Blob Container, reading individual Blob object data, downloading and uploading files to Blob Storage, and generating time-limited signed GET and PUT URLS to allow a separate application (such as a Vue frontend) to upload and retrieve objects.

### Endpoints

See the [Swagger doc](./swagger.yaml) for details of the API endpoints.

### Generating signed URLs for uploading

When calling the endpoint to generate a signed PUT URL, the API will return two attributes - `url` and `filename`, e.g.

```
{
  "url": "https:\/\/eydiag108.blob.core.windows.net\/bootdiagnostics-eyfcn-370d95b2-6834-40be-b6c5-fa7657397dc6\/b2d255db-2d9d-4992-aebe-a0201d04de52.gif?sv=2017-04-17\u0026sr=b\u0026se=2018-11-29T16:19:11Z\u0026sp=w\u0026spr=https\u0026sig=fiJGnViY%2BQ2WKFZ2GUSbAHIiLm6%2F9BCtOHf2UnOg6UQ%3D",
  "filename": "b2d255db-2d9d-4992-aebe-a0201d04de52.gif"
}
```

`url` is a time-limited endpoint (currently set to a one hour expiry) you can use to PUT the file you wish to upload.

`filename` indicates the uniquely-generated filename that the object will be stored under in the container, and is the value you should use to store in a DB (or wherever) on successful upload.

When performing your PUT request, it **must** include the following header:

`x-ms-blob-type: BlockBlob`

The Azure PUT request will return a 201 status on successful upload.